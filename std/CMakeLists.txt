cmake_minimum_required (VERSION 3.8)

project(std LANGUAGES CXX)

add_library(std STATIC
   stdio.cpp
   )
   
target_link_libraries(std PUBLIC
   gcc
   )   
   
target_include_directories(std INTERFACE
    "${PROJECT_SOURCE_DIR}")
