
function (add_app target src link)
   message(STATUS "Buillding App ${target} with")

   foreach(f ${src})
      message(STATUS ${f})
   endforeach()

   
   foreach(f ${link})
      message(STATUS ${f})
   endforeach()
   
   execute_process(COMMAND ${CMAKE_CXX_COMPILER} ${CFLAGS} -print-file-name=crtbegin.o
                  OUTPUT_VARIABLE CRTBEGIN_OBJ
                  OUTPUT_STRIP_TRAILING_WHITESPACE)
                  
   if(NOT ${CRTBEGIN_OBJ} STREQUAL "")
      message(STATUS "${target} found ${CRTBEGIN_OBJ}")
   endif()     
                  
   execute_process(COMMAND ${CMAKE_CXX_COMPILER} ${CFLAGS} -print-file-name=crtend.o
                  OUTPUT_VARIABLE CRTEND_OBJ
                  OUTPUT_STRIP_TRAILING_WHITESPACE)
                  
   if(NOT ${CRTEND_OBJ} STREQUAL "")
      message(STATUS "${target} found ${CRTEND_OBJ}")
   endif()  

    add_executable(${target} 
      ${src}
      ${CRTBEGIN_OBJ}
      ${CRTEND_OBJ}
      ${LINKER_SCRIPT}
      )
      
    target_link_libraries (${target} 
      ${link}
      x86
      std
      boot
      )
      
   set_target_properties (${target}
      PROPERTIES LINK_FLAGS "-Wl,-Map,${target}.map,--oformat=binary -T ${LINKER_SCRIPT}")  
            
   add_custom_command(TARGET ${target} POST_BUILD
      COMMAND objdump -Dsfe -mi386 -b binary -M intel $<TARGET_FILE:${target}> > ${target}.list
      BYPRODUCTS "${target}.list")
endfunction ()
