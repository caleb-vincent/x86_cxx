;  Copyright © 2019 Caleb Vincent
;
;  Permission is hereby granted, free of charge, to any person obtaining a
;  copy of this software and associated documentation files (the "Software"),
;  to deal in the Software without restriction, including without limitation
;  the rights to use, copy, modify, merge, publish, distribute, sublicense,
;  and/or sell copies of the Software, and to permit persons to whom the
;  Software is furnished to do so, subject to the following conditions:
;
;  The above copyright notice and this permission notice shall be included
;  in all copies or substantial portions of the Software.
;
;  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;  DEALINGS IN THE SOFTWARE.

section .boot
bits 16
global boot
boot:
   mov ax, 0x3
   int 0x10

; Load some from disk, an arbitrary amount for now

   mov [disk],dl
   mov ah, 0x2          ;  read sectors
   mov al, 48            ;  sectors to read
   mov ch, 0            ;  cylinder idx
   mov dh, 0            ;  head idx
   mov cl, 2            ;  sector idx
   mov dl, [disk]       ;  disk idx
   mov bx, copy_target  ;  target pointer
   int 0x13

   cli

 ; Setup a GDT

   xor ax, ax
   mov ds, ax
   lgdt [gdt_pointer]

 ; Protected mode

   mov eax, cr0
   or eax,0x1
   mov cr0, eax

; Prepare for jump into C++

   mov ax, DATA_SEG
   mov ds, ax
   mov es, ax
   mov fs, ax
   mov gs, ax
   mov ss, ax
   jmp CODE_SEG:cxx

; Our GDT, Segment from 0-4GB
gdt_start:
   dq 0x0
gdt_code:
   dw 0xFFFF
   dw 0x0
   db 0x0
   db 10011010b
   db 11001111b
   db 0x0
gdt_data:
   dw 0xFFFF
   dw 0x0
   db 0x0
   db 10010010b
   db 11001111b
   db 0x0
gdt_end:
gdt_pointer:
   dw gdt_end - gdt_start
   dd gdt_start
disk:
   db 0x0
CODE_SEG equ gdt_code - gdt_start
DATA_SEG equ gdt_data - gdt_start

times 510 - ($-$$) db 0
dw 0xAA55

bits 32

section .start
copy_target:
cxx:
   ; Run our loaded C++ _stage1() (assuming it fit in the sectors we loaded)
   mov esp, kernel_stack_top
   call _init
   extern _stage1
   mov DWORD [0xb8000], 0x07310753  ; Print "S1"
   call _stage1
   call _fini
   cli
   hlt

   
section .init_pre
global _init
_init:
   push ebp
   mov  ebp, esp
   mov DWORD [0xb8000], 0x07690743  ; Print "Ci"


section .fini_pre
global _fini
_fini:
   push ebp
   mov ebp, esp
   mov DWORD [0xb8000], 0x076E0743  ; Print "Cn"

   
section .init_post
   mov DWORD [0xb8000], 0x07690744  ; Print "Di"
   pop ebp
   ret

section .fini_post
   mov DWORD [0xb8000], 0x076E0744  ; Print "Dn"
   pop ebp
   ret

   
 ; Setup a 16K stack in it's own section
section .bss
align 4
kernel_stack_bottom: equ $
   resb 16384 ; 16 KB
   align 4
kernel_stack_top:
