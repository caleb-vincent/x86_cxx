cmake_minimum_required (VERSION 3.0)

set(ASM_EXT asm)
enable_language (ASM_NASM)
                
# Strip any existing NASM Flags (specifically -f elf64)
set(CMAKE_ASM_NASM_COMPILE_OBJECT "<CMAKE_ASM_NASM_COMPILER> <INCLUDES> <FLAGS> -o <OBJECT> <SOURCE>")

add_library( stage0 OBJECT stage0.asm)
target_compile_options(stage0 PRIVATE "-f elf32")

add_library(boot STATIC 
    $<TARGET_OBJECTS:stage0>)

add_custom_command(TARGET boot POST_BUILD
    COMMAND objdump -Dsfe -b binary -mi386 -M intel $<TARGET_FILE:boot> > boot.list
    BYPRODUCTS "boot.list"
)
