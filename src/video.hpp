#pragma once

// Copyright © 2019 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


#include <cstdint>

class Video
{
public:

   enum class Color : uint8_t
   {
      BLACK          =  0x00,
      BLUE           =  0x01,
      GREEN          =  0x02,
      CYAN           =  0x03,
      RED            =  0x04,
      MAGENTA        =  0x05,
      BROWN          =  0x06,
      LIGHT_GREY     =  0x07,
      DARK_GREY      =  0x08,
      LIGHT_BLUE     =  0x09,
      LIGHT_GREEN    =  0x0A,
      LIGHT_CYAN     =  0x0B,
      LIGHT_RED      =  0x0C,
      LIGHT_MAGENTA  =  0x0D,
      LIGHT_BROWN    =  0x0E,
      WHITE          =  0x0F
   };

   uint16_t* GetBuffer();

   void ShowCursor(bool show);

private:
   enum class Mode
   {
      NONE           =  0x00,
      COLOR_NARROW   =  0x10,
      COLOR          =  0x20,
      MONOCHROME     =  0x30
   };

   struct RegisterIndex
   {
      enum : std::uint8_t
      {
         HORZ_TOTAL           =  0x00,
         END_HORZ_DISP        =  0x01,
         END_HORZ_BLANK       =  0x03,
         START_HORZ_RETRACE   =  0x04,
         END_HORZ_RETRACE     =  0x05,
         VERT_TOTAL           =  0x06,
         OVERFLOW             =  0x07,
         PRESET_ROW_SCAN      =  0x08,
         MAX_SCAN_LINE        =  0x09,
         CURSOR_START         =  0x0A,
         CURSOR_END           =  0x0B,
         START_ADDR_HIGH      =  0x0C,
         START_ADDR_LOW       =  0x0D,
         CURSOR_LOC_HIGH      =  0x0E,
         CURSOR_LOC_LOW       =  0x0F,
         VERT_RETRACE_START   =  0x10,
         VERT_RETRACE_END     =  0x11,
         VERT_Display_END     =  0x12,
         OFFSET               =  0x13,
         UNDERLINE_LOC        =  0x14,
         START_VERT_BLANK     =  0x15,
         END_VERT_BLANK       =  0x16,
         CRTC_MODE_CONTROL    =  0x17,
         LINE_COMPARE         =  0x18
      };
   };

   constexpr static auto MODE_MASK{ 0x30 };
   constexpr static auto COLOR_BUFFER_ADDRESS{ 0xB8000 };
   constexpr static auto MONO_BUFFER_ADDRESS{ 0xB0000 };

   constexpr static auto INDEX_REGISTER_PORT{ 0x3D4 };
   constexpr static auto REGISTER_DATA_PORT{ 0x3D5 };

   constexpr static auto CURSOR_DISABLE_BIT = 0x05;

   static uint16_t* SelectBuffer();

   uint16_t*const m_pBuffer{ SelectBuffer() };
};
