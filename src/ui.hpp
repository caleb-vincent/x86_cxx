#pragma once

// Copyright © 2019 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

#include "video.hpp"

#include <cstddef>
#include <cstdint>

class Ui
{
public:
   constexpr static auto TAB_WIDTH{4};

   Ui(Video& video);

   Ui& operator <<(char const* text);
   Ui& operator <<(Video::Color color);

   void putc(std::uint32_t row, std::uint32_t col, char c);
   char getc(std::uint32_t row, std::uint32_t col);

private:
   using ColorByte_t = std::uint8_t;
   using CharacterWord_t = std::uint16_t;

   static constexpr ColorByte_t ColorByte(Video::Color fg, Video::Color bg = Video::Color::BLACK);
   static constexpr CharacterWord_t CharacterWord(char c, ColorByte_t color);
   Video& m_video;

   const std::uint32_t m_width;
   const std::uint32_t m_height;

   Video::Color m_currColor;
   std::size_t m_videoBufferPosition;

};
