// Copyright © 2019 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

#include "serial.hpp"

#include "io.hpp"

#include <cstddef>
#include <cstdio>

Serial trace(IoPort::COM1);

Serial::Serial(IoPort const comPort) :
m_port{ comPort }
{
   outb(m_port + 1u, 0x00);    // Disable all interrupts
   outb(m_port + 3u, 0x80);    // Enable DLAB (set baud rate divisor)
   outb(m_port + 0u, 0x01);    // Set divisor to 1 (lo byte) 115200 baud
   outb(m_port + 1u, 0x00);    //                  (hi byte)
   outb(m_port + 3u, 0x03);    // 8 bits, no parity, one stop bit
   outb(m_port + 2u, 0xC7);    // Enable FIFO, clear them, with 14-byte threshold
   outb(m_port + 4u, 0x03);    // RTS/DSR set
}

Serial& Serial::operator<<(char const*const str)
{
   writeOut(str);
   return *this;
}

Serial& Serial::operator<<(unsigned int const i)
{
   char c[32];
   std::snprintf(&c[0], sizeof(c) - 1, "%u", i);
   writeOut(&c[0]);
   return *this;
}

bool Serial::isTrasmitQueueEmpty()
{
   return inb(m_port + 5) & 0x20;
}

void Serial::writeOut(char const* data)
{
   while(isTrasmitQueueEmpty() && *data != '\0')
   {
      outb(m_port, static_cast<std::uint8_t>(*data++));
   }
}
