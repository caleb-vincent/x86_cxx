// Copyright © 2019 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

#include "ui.hpp"

#include "video.hpp"

Ui::Ui(Video& video) :
   m_video{ video },
   m_width{ 80 },
   m_height{ 25 },
   m_currColor{ Video::Color::LIGHT_GREY },
   m_videoBufferPosition{ 0 }
{}

Ui& Ui::operator <<(char const* text)
{
   for(std::size_t i{0}; text[i] != '\0' && m_videoBufferPosition < m_width * m_height - 1; ++i, ++m_videoBufferPosition)
   {
      switch(text[i])
      {
      case '\b':
         if(m_videoBufferPosition > 0)
         {
            m_video.GetBuffer()[--m_videoBufferPosition] = CharacterWord(' ', ColorByte(m_currColor));
            --m_videoBufferPosition;
         }
         break;
      case '\t':
         m_videoBufferPosition = m_videoBufferPosition + (TAB_WIDTH - (m_videoBufferPosition % m_width) % TAB_WIDTH) - 1;
         break;
      case '\n':
         // Bit of odd behavior here.
         //    If we just filled to the end of a line, we are on the next one already, and adding a '\n' will result in a
         //    (possibly unexpected) additional newline
         m_videoBufferPosition = (((m_videoBufferPosition / m_width) + 1) * m_width) + (m_videoBufferPosition % m_width) - 1;
         break;
      case '\r':
         m_videoBufferPosition = ((m_videoBufferPosition - 1) / m_width) * m_width - 1;
         break;
      default:
         m_video.GetBuffer()[m_videoBufferPosition] = CharacterWord(text[i], ColorByte(m_currColor));
      };
   }
   return *this;
}

void Ui::putc(std::uint32_t const row, std::uint32_t const col, char const c)
{
   if(row < m_height && col < m_width)
   {
      m_video.GetBuffer()[row * m_width + col] = CharacterWord(c, ColorByte(m_currColor));
   }
}

char Ui::getc(std::uint32_t const row, std::uint32_t const col)
{
   if(row < m_height && col < m_width)
   {
      return static_cast<char>(m_video.GetBuffer()[row * m_width + col]);
   }
   return 0;
}

constexpr Ui::ColorByte_t Ui::ColorByte(Video::Color const fg, Video::Color const bg /*= Video::Color::BLACK*/)
{
   return static_cast<std::uint8_t>(((static_cast<std::uint8_t>(bg) & 0x0F) << 4) | (static_cast<std::uint8_t>(fg) & 0x0F));
}

constexpr Ui::CharacterWord_t Ui::CharacterWord(char c, Ui::ColorByte_t color)
{
   return static_cast<uint16_t>(color << 8) | static_cast<std::uint16_t>(c);
}

Ui& Ui::operator <<(Video::Color const color)
{
   m_currColor = color;
   return *this;
}
