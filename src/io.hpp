#pragma once

// Copyright © 2019 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

#include <cstdint>
#include <cstddef>

template<typename T>
static constexpr inline T SetBit(T const& number, std::size_t const bit)
{
   return number | static_cast<T>(1 << (bit));
}

template<typename T>
static constexpr inline T ClearBit(T const& number, std::size_t const bit)
{
   return number & static_cast<T>(~(1 << bit));
}

enum class IoPort : std::uint16_t
{
   PIC1_COMMAND         =  0x0020,
   PIC1_DATA            =  0x0021,
   CMOS_INDEX_REGISTER  =  0x0070,
   CMOS_DATA_REGISTER   =  0x0071,
   PIC2_COMMAND         =  0x00A0,
   PIC2_DATA            =  0x00A1,
   COM4                 =  0x02E8,
   COM2                 =  0x02F8,
   VGA_INDEX_REGISTER   =  0x03D4,
   VGA_DATA_REGISTER    =  0x03D5,
   COM3                 =  0x03E8,
   COM1                 =  0x03F8,
};

//! @todo enable_if is_integral
template<typename T>
IoPort operator+(IoPort const& port, T const& n)
{
   return static_cast<IoPort>(static_cast<T>(port) + n);
}

static inline void outb(IoPort const port, uint8_t const value)
{
   asm volatile ("outb %1, %0" : : "dN" (port), "a" (value));
}

static inline uint8_t inb(IoPort const port)
{
    uint8_t ret;
    asm volatile ( "inb %1, %0" : "=a"(ret) : "Nd"(port) );
    return ret;
}
