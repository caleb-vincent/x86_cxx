// Copyright © 2019 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.


#include "video.hpp"

#include "bios.hpp"
#include "io.hpp"
#include "memory.hpp"

std::uint16_t* Video::GetBuffer()
{
   return m_pBuffer;
}

void Video::ShowCursor(bool const show)
{
   // Ensure only the Cursor Disable bit (5) is toggled
   if(show)
   {
      outb(IoPort::VGA_INDEX_REGISTER, RegisterIndex::CURSOR_START);
      outb(IoPort::VGA_DATA_REGISTER, ClearBit(inb(IoPort::VGA_DATA_REGISTER), CURSOR_DISABLE_BIT));
   }
   else
   {
      outb(IoPort::VGA_INDEX_REGISTER, RegisterIndex::CURSOR_START);
      outb(IoPort::VGA_DATA_REGISTER, SetBit(inb(IoPort::VGA_DATA_REGISTER),  CURSOR_DISABLE_BIT));
   }
}

uint16_t* Video::SelectBuffer()
{
   switch(static_cast<Mode>(Memory::Peek<std::uint16_t>(BDA::VIDEO_MODE) & MODE_MASK))
   {
   case Mode::COLOR_NARROW:
   case Mode::COLOR:
      return reinterpret_cast<uint16_t*>(COLOR_BUFFER_ADDRESS);
   case Mode::MONOCHROME:
      return reinterpret_cast<uint16_t*>(MONO_BUFFER_ADDRESS);
   default:
      return static_cast<uint16_t*>(nullptr);
   }
}
