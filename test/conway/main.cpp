// Copyright © 2019 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

#include "io.hpp"
#include "memory.hpp"
#include "serial.hpp"
#include "ui.hpp"
#include "video.hpp"

#include <cstddef>
#include <cstdint>

static void ShortPause()
{
   outb(IoPort::CMOS_INDEX_REGISTER, 0);
   auto const secs{ inb(IoPort::CMOS_DATA_REGISTER) };
   while(inb(IoPort::CMOS_DATA_REGISTER) == secs){}
}

namespace
{
   void Conway(Ui& ui)
   {
      for( std::size_t i {0}; true; ++i)
      {
         trace << "Iteration: " <<  i << "\n";
         ui << Video::Color::LIGHT_GREY;
         for(std::uint32_t x{0}; x < 80; ++x)
         {
            ui.putc(0, x, ((i % 80) == x) ? '*' : ' ');
         }

         for(std::uint32_t x{0}; x < 80; ++x)
         {
            for(std::uint32_t y{1}; y < 25; ++y)
            {
               std::uint32_t neighbors{0};

               std::uint32_t const xl {x == 0 ? 79 : x - 1};
               std::uint32_t const xr {x == 79 ? 0 : x + 1};
               std::uint32_t const yu {y == 1 ? 24 : y - 1};
               std::uint32_t const yd {y == 24 ? 1 : y + 1};

               neighbors += ui.getc(yu, xl) != ' ' ? 1u : 0u;
               neighbors += ui.getc(yd, xl) != ' ' ? 1u : 0u;
               neighbors += ui.getc(yu, xr) != ' ' ? 1u : 0u;
               neighbors += ui.getc(yd, xr) != ' ' ? 1u : 0u;
               neighbors += ui.getc(yu, x) != ' ' ? 1u : 0u;
               neighbors += ui.getc(yd, x) != ' ' ? 1u : 0u;
               neighbors += ui.getc(y, xr) != ' ' ? 1u : 0u;
               neighbors += ui.getc(y, xl) != ' ' ? 1u : 0u;

               switch(neighbors)
               {
               case 2:
                  ui << Video::Color::DARK_GREY;
                  break;
               case 3:
                  ui << Video::Color::BROWN;
                  break;
               default:
                  ui << Video::Color::LIGHT_GREY;
               }

               if(ui.getc(y, x) == ' ')
               {
                  if(neighbors == 3)
                  {
                     ui.putc(y, x, '#');
                  }
               }
               else
               {
                  if(neighbors < 2 || neighbors > 3)
                  {
                     ui.putc(y, x, ' ');
                  }
                  else
                  {
                     ui.putc(y, x, '#');
                  }
               }
            }
         }
         ShortPause();
      }
   }
}

extern "C" void _stage1()
{   
   constexpr auto text{ "Running C++!" };
   
   Video video;
   Ui ui(video);

   video.ShowCursor(false);

   
   trace << "Blanking screen\n";
   
   for(std::uint32_t x{0}; x < 80; ++x)
   {
      for(std::uint32_t y{0}; y < 25; ++y)
      {
         ui.putc(y,x, ' ');
      }
   }
   
   trace << "Testing screen width\n";

   ui << Video::Color::DARK_GREY;
   ui << "00000000001111111111222222222233333333334444444444555555555566666666667777777777";
   ui << "01234567890123456789012345678901234567890123456789012345678901234567890123456789";
   
   trace << "Testing tabs\n";
   
   ui << Video::Color::LIGHT_BLUE << "0\t1\t2\t3\t4\t5\t6\t7\t8\t9\tA\tB\tC\tD\tE\tF\r\n";
   ui << Video::Color::LIGHT_BLUE << "00\t11\t22\t33\t44\t55\t66\t77\t88\t99\tAA\tBB\tCC\tDD\tEE\tFF\r\n";
   ui << Video::Color::LIGHT_BLUE << "000\t111\t222\t333\t444\t555\t666\t777\t888\t999\tAAA\tBBB\tCCC\tDDD\tEEE\tFFF\r\n";
   ui << Video::Color::LIGHT_BLUE << "0000\t1111\t2222\t3333\t4444\t5555\t6666\t7777\t8888\r\n";
   ui << Video::Color::LIGHT_BLUE << "00000\t11111\t22222\t33333\t44444\t55555\t66666\t77777\t88888\r\n";
   ui << Video::Color::BROWN << "----------------------------------------------------------------------0123456\t";
   
   trace << "Testing colors\n";
   
   ui << Video::Color::LIGHT_GREY << text << "\r\n";
   ui << Video::Color::RED << text << "\r\n";
   ui << Video::Color::GREEN<< text << "\r\n";
   ui << Video::Color::BLUE<< text << "\r\n";
   ui << Video::Color::LIGHT_MAGENTA << text << "\r\n";
   ui << Video::Color::BROWN << "?????????\n" << "!\r\n";
   
   trace << "Testing backspace";
   
   ui << "~" << "\b";

   trace << "Testing screen extents\n";
   
   for(std::uint32_t x{0}; x < 25; ++x)
   {
      ui.putc(x, x, '+');
      ui.putc(x, 79-x, ui.getc(x, x));
   }
   
   trace << "Testing corners\n";
   
   ui.putc(0, 0, '#');
   ui.putc(0, 79, '@');
   ui.putc(24, 0, '%');
   ui.putc(24, 79, '&');

   ShortPause();
   ui << Video::Color::LIGHT_GREY;
   
   trace << "Starting Conway's Game of Life\n";   
   
   Conway(ui);
}
